package com.example.venues.api

import com.example.venues.BuildConfig
import com.example.venues.data.VenueDetailsResponse
import com.example.venues.data.VenueSearchResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.text.SimpleDateFormat
import java.util.*

interface FourSquareService {

    @GET("venues/search")
    fun searchVenues(
        @Query("near") near: String,
        @Query("limit") limit: Int = LIMIT,
        @Query("client_id") clientId: String = BuildConfig.CLIENT_ID,
        @Query("client_secret") clientSecret: String = BuildConfig.CLIENT_SECRET,
        @Query("v") date: String = SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date())
    ): Observable<VenueSearchResponse>

    @GET("venues/{venue_id}")
    fun getVenueDetails(
        @Path("venue_id") venueId: String,
        @Query("client_id") clientId: String = BuildConfig.CLIENT_ID,
        @Query("client_secret") clientSecret: String = BuildConfig.CLIENT_SECRET,
        @Query("v") date: String = SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(Date())

    ): Observable<VenueDetailsResponse>

    companion object {
        const val LIMIT = 10
    }
}