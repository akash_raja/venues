package com.example.venues.ui.search

import android.util.Log
import com.example.venues.api.FourSquareService
import com.example.venues.ui.search.database.VenueDao
import com.example.venues.ui.search.database.VenueDataStore
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class VenueRepository @Inject constructor(private val dao: VenueDao, private val fourSquareService: FourSquareService) {

    private fun syncFromNetwork(search: String): Completable {
        return Completable.create { emitter ->
            fourSquareService.searchVenues(search).subscribeOn(Schedulers.io()).subscribe({ venues ->
                dao.insertList(venues.response.venues.map {
                    VenueDataStore(
                        it.id,
                        search.toLowerCase(),
                        it.name,
                        it.location.formattedAddress.toString()
                    )
                }).subscribe({
                    emitter.onComplete()
                }, {
                    emitter.onError(it)
                })
            }, {
                emitter.onError(it)
            })
        }
    }

    fun fetchData(searchInput: String): Single<List<VenueDataStore>> {
        Log.e("XXXX", "fetch called with $searchInput")
        return Single.create { emitter ->
            dao.loadVenueData(searchInput).subscribe { listOfVenues ->
                if (listOfVenues.isEmpty()) {
                    syncFromNetwork(searchInput).subscribe({
                    }, {
                        emitter.onError(it)
                    })
                } else {
                    emitter.onSuccess(listOfVenues)
                }
            }
        }
    }
}