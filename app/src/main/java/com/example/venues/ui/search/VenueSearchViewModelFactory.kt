package com.example.venues.ui.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.venues.api.FourSquareService
import com.example.venues.ui.search.database.VenueDatabase
import javax.inject.Inject

class VenueSearchViewModelFactory @Inject constructor(private val venueRepository: VenueRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VenueSearchViewModel::class.java)) {
            return VenueSearchViewModel(venueRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}