package com.example.venues.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.venues.R
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_venue_details.*
import javax.inject.Inject

class VenueDetailsFragment : DaggerFragment() {

    private val args: VenueDetailsFragmentArgs by navArgs()

    @Inject
    lateinit var venueDetailsViewModelFactory: VenueDetailsViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_venue_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(this, venueDetailsViewModelFactory).get(VenueDetailsViewModel::class.java)
        viewModel.getVenueDetails(args.venueId)

        viewModel.uiData.observe(viewLifecycleOwner, Observer {
            when (it) {
                VenueDetailsUIState.Loading -> progress.visibility = View.VISIBLE

                is VenueDetailsUIState.Success -> {
                    progress.visibility = View.GONE
                    title.text = it.data.title
                    description.text = it.data.description
                    rating.text = it.data.rating
                    address.text = it.data.address
                    contact.text = it.data.contact

                    Glide.with(this).load(it.data.photoUrl).error(R.drawable.fallback).into(photo)
                }
                VenueDetailsUIState.Failed -> {
                    progress.visibility = View.GONE
                }
            }
        })
    }
}
