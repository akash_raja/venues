package com.example.venues.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.venues.R
import com.example.venues.ui.search.database.VenueDataStore
import kotlinx.android.synthetic.main.venue_item.view.*

class VenueAdapter(private val listener: (String) -> Unit) :
    ListAdapter<VenueDataStore, VenueViewHolder>(object : DiffUtil.ItemCallback<VenueDataStore>() {

        override fun areItemsTheSame(oldItem: VenueDataStore, newItem: VenueDataStore): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: VenueDataStore, newItem: VenueDataStore): Boolean =
            oldItem == newItem
    }) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VenueViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.venue_item, parent, false)
        return VenueViewHolder(view)
    }

    override fun onBindViewHolder(holder: VenueViewHolder, position: Int) {
        val venue = getItem(position)
        holder.bindTo(venue)
        holder.itemView.setOnClickListener { listener(venue.id) }
    }
}

class VenueViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun bindTo(venue: VenueDataStore) {
        view.venue_name.text = venue.title
        view.venue_location.text = venue.address
    }
}