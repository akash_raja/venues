package com.example.venues.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.venues.ui.search.database.VenueDataStore
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class VenueSearchViewModel(private val repository: VenueRepository) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val _uiState = MutableLiveData<VenueSearchUIState>()
    val uiState: LiveData<VenueSearchUIState> = _uiState

    fun search(search: String, isNetworkAvailable: Boolean) {
        val searchString = search.trim().toLowerCase()
        _uiState.value = VenueSearchUIState.Loading

        compositeDisposable.add(repository.fetchData(searchString).subscribeOn(Schedulers.io()).subscribe({ venueList ->
            if (venueList.isNotEmpty()) {
                _uiState.postValue(VenueSearchUIState.Success(venueList))
            } else if (!isNetworkAvailable) {
                _uiState.postValue(VenueSearchUIState.Failed)
            }
        }, {
            _uiState.postValue(VenueSearchUIState.Failed)
        }))
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}

sealed class VenueSearchUIState {
    object Loading : VenueSearchUIState()
    class Success(val venueList: List<VenueDataStore>) : VenueSearchUIState()
    object Failed : VenueSearchUIState()
}