package com.example.venues.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.venues.R
import com.example.venues.ui.adapter.VenueAdapter
import com.example.venues.util.Network.isNetworkAvailable
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_venue_search.*
import javax.inject.Inject


class VenueSearchFragment : DaggerFragment() {

    @Inject
    lateinit var factory: VenueSearchViewModelFactory

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_venue_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val venueSearchViewModel = ViewModelProvider(this, factory).get(VenueSearchViewModel::class.java)

        val venueAdapter = VenueAdapter {
            val action = VenueSearchFragmentDirections.actionVenueSearchFragmentToVenueDatailsFragment(it)
            if (isNetworkAvailable(requireContext())) {
                findNavController().navigate(action)
            } else {
                Toast.makeText(requireContext(), getString(R.string.check_internet), Toast.LENGTH_SHORT).show()
            }
        }

        submit_button.setOnClickListener {
            val search = search_venue.text.toString()
            venueSearchViewModel.search(search, isNetworkAvailable(requireContext()))
            search_venue.onEditorAction(EditorInfo.IME_ACTION_DONE)
        }

        venue_rv.adapter = venueAdapter
        venueSearchViewModel.uiState.observe(viewLifecycleOwner, Observer { uiState ->
            when (uiState) {
                VenueSearchUIState.Loading -> {
                    loading_progress_bar.visibility = View.VISIBLE
                    venue_rv.visibility = View.INVISIBLE
                }
                is VenueSearchUIState.Success -> {
                    loading_progress_bar.visibility = View.GONE
                    venueAdapter.submitList(uiState.venueList)
                    venue_rv.visibility = View.VISIBLE
                }
                VenueSearchUIState.Failed -> {
                    loading_progress_bar.visibility = View.GONE
                    Snackbar.make(submit_button, R.string.failed_message, Snackbar.LENGTH_SHORT).show()
                }
            }
        })
    }
}

