package com.example.venues.ui.search.database

import androidx.room.*
import io.reactivex.Completable
import io.reactivex.Observable

@Dao
interface VenueDao {

    @Query("Select * from VenueDataStore where searchString = :search")
    fun loadVenueData(search: String): Observable<List<VenueDataStore>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertList(venue: List<VenueDataStore>): Completable
}