package com.example.venues.ui.details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import rx.schedulers.Schedulers

class VenueDetailsViewModel(private val repository: VenueDetailsRepository) : ViewModel() {

    private val venueDetailUiData = MutableLiveData<VenueDetailsUIState>()
    val uiData = venueDetailUiData

    fun getVenueDetails(venueId: String) {
        if (venueDetailUiData.value == null) {
            venueDetailUiData.value = VenueDetailsUIState.Loading
            repository.getVenueDetails(venueId).subscribeOn(Schedulers.io()).subscribe({ uiState ->
                venueDetailUiData.postValue(uiState)
            }, {
                venueDetailUiData.postValue(VenueDetailsUIState.Failed)
            })
        }
    }
}

data class VenueDetailsUIModel(
    val title: String,
    val description: String,
    val rating: String,
    val contact: String,
    val address: String,
    val photoUrl: String
)

sealed class VenueDetailsUIState {
    object Loading : VenueDetailsUIState()
    class Success(val data: VenueDetailsUIModel) : VenueDetailsUIState()
    object Failed : VenueDetailsUIState()
}