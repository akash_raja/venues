package com.example.venues.ui.details

import com.example.venues.api.FourSquareService
import io.reactivex.schedulers.Schedulers
import rx.Single
import javax.inject.Inject

class VenueDetailsRepository @Inject constructor(private val fourSquareService: FourSquareService) {

    fun getVenueDetails(venueId: String): Single<VenueDetailsUIState> {
        return Single.create { emitter ->
            fourSquareService.getVenueDetails(venueId).subscribeOn(Schedulers.io()).subscribe({ venueDetails ->
                val venue = venueDetails.response.venue
                val uiData = VenueDetailsUIModel(
                    title = venue.name ?: "",
                    description = venue.description ?: "",
                    rating = venue.likes?.summary ?: "",
                    address = venue.location?.formattedAddress.toString(),
                    contact = venue.contact.toString(),
                    photoUrl = "${venue.bestPhoto?.prefix}original${venue.bestPhoto?.suffix}")

                emitter.onSuccess(VenueDetailsUIState.Success(uiData))
            }, {
                emitter.onError(it)
            })
        }
    }
}