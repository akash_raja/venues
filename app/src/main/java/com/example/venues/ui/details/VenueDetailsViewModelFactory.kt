package com.example.venues.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class VenueDetailsViewModelFactory @Inject constructor(private val detailsRepository: VenueDetailsRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VenueDetailsViewModel::class.java)) {
            return VenueDetailsViewModel(detailsRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}